<?php
include_once "car.php";
include_once "engine.php";

class Audi extends Car {
  
  private $getEngineStat = false;
  const MODEL = 'Audi';
  public $speed = 0;
  
  function __construct($title, $color, $maxSpeed, $engine){
    parent::__construct($title, $color, $maxSpeed, $engine);
  }
  
  function turnOn(){
    if($this->getEngineStat){
      $this->turnEngine(Audi::MODEL . ' ' . $this->title . ' Turn off by button ');
      $this->getEngineStat = !$this->getEngineStat;
    } else {
      $this->turnEngine(Audi::MODEL . ' ' . $this->title . ' Turn on by button ');
      $this->getEngineStat = !$this->getEngineStat;
    }
  }
}

$eng = new Engine('audi-sport-v200', 750);

$audi1 = new Audi('R8', 'Blue', '250', $eng);

echo 'Turn Up'   . $audi1->turnOn() . '<br>';


echo 'Speed '   . $audi1->speed      . '<br>';
echo 'speedUp ' . $audi1->speedUp(250) . '<br>';
echo 'Speed '   . $audi1->speed       . '<br>';

echo 'speedDown ' . $audi1->speedDown() . '<br>';
echo 'Speed '     . $audi1->speed       . '<br>';

echo 'speedUp ' . $audi1->speedUp(200) . '<br>';
echo 'Speed '   . $audi1->speed      . '<br>';

echo 'Turn down' . $audi1->turnOn() . '<br>';
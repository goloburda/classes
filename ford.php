<?php
include_once "car.php";
include_once "engine.php";

class Ford extends Car {
  
  private $getEngineStat = false;
  const MODEL = 'Ford';
  public $speed = 0;
  
  function __construct($title, $color, $maxSpeed, $engine){
    parent::__construct($title, $color, $maxSpeed, $engine);
  }
  
  function turnOn(){
    if($this->getEngineStat){
      $this->turnEngine(Ford::MODEL . ' ' . $this->title . ' Turn off by key ');
      $this->getEngineStat = !$this->getEngineStat;
    } else {
      $this->turnEngine(Ford::MODEL . ' ' . $this->title . ' Turn on by key ');
      $this->getEngineStat = !$this->getEngineStat;
    }
  }
}

$eng = new Engine('ford-cruise-v200', 750);

$ford1 = new Ford('Focus', 'Pink', '150', $eng);

echo 'Turn Up'   . $ford1->turnOn() . '<br>';


echo 'Speed '   . $ford1->speed      . '<br>';
echo 'speedUp ' . $ford1->speedUp(250) . '<br>';
echo 'Speed '   . $ford1->speed       . '<br>';

echo 'speedDown ' . $ford1->speedDown() . '<br>';
echo 'Speed '     . $ford1->speed       . '<br>';

echo 'speedUp ' . $ford1->speedUp(200) . '<br>';
echo 'Speed '   . $ford1->speed      . '<br>';

echo 'Turn down' . $ford1->turnOn() . '<br>';
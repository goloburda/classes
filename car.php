<?php
//spl_autoload_register(function ($class_name) {
//    include $class_name . '.php';
//});
  include_once "engine.php";


abstract class Car extends Engine{
  public $title;
  public $color;
  public $maxSpeed;
  public $engine;
  
  function __construct($title, $color, $maxSpeed, $engine){
    $this->title    = $title;
    $this->color    = $color;
    $this->maxSpeed = $maxSpeed;
    $this->engine   = $engine;
  }
  
  abstract function turnOn();  // Ключь в зажигании on/off
  
  
  protected function turnEngine($text){ // change engine state depends on turnOn
    $this->engine->getState($text);
  }
  
  function speedUp($value){
    if( ($this->speed + $value) > $this->maxSpeed){
      $this->speed = $this->maxSpeed;
      return $this->speed;
    }
    else {
      $this->speed += $value;
      return $this->speed;
    }
  }
  
  function speedDown(){
    if (($this->speed - 20) <= 0) {
      $this->speed = 0;
    }
    else {
      $this->speed -= 20;
      return $this->speed;
    }
  }
}
<?php


class Engine {
  private $model;
  private $power;
  private $state = false;
  
  function __construct($model, int $power){
    $this->model  = $model;
    $this->power = $power;
  }
  
  function getModel(){
    return $this->model;
  }
  
  function getPower(){
    return $this->power;
  }
  
  function getState($text){
    $this->setState();
    echo $this->state ? $text : $text;
  }
  
  function setState(){
    $this->state = !$this->state;
  }
}






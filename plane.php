<?php

class Plane {
  public $model;
  public $brand;
  public $ammunition;
  public $queue;
  public $amm;
  
  function __construct(string $model, string $brand, int $ammunition, int $queue) {
    $this->model = $model;
    $this->brand = $brand;
    $this->ammunition = $ammunition;
    $this->queue = $queue;
    $this->amm = $ammunition;
  }
  
  function reload() {
    $this->ammunition = $this->amm;
    echo 'AMM remaing: ' . $this->ammunition . '<br>';
  }
  
  function shoot() {
    if ($this->ammunition < 1) {
      echo 'No ammo left' . '<br>';
    }
    else {
      $this->ammunition--;
      echo 'One shoot AMM remaing: ' . $this->ammunition . '<br>';
    }
  }
  
  function shootQueue() {
    if ($this->ammunition < $this->queue) {
      echo 'No ammo left' . '<br>';
    } 
    else {
      $this->ammunition -= $this->queue;
      echo 'Queue shoot AMM remaing: ' . $this->ammunition . '<br>';
    }
  }
}

$plane = new Plane('TY 145', 'RUS', 1, 5);
$plane->shoot();
$plane->shoot();
$plane->shootQueue();
$plane->shootQueue();
$plane->shootQueue();
$plane->reload();

